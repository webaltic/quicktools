<?php

namespace Quicktools;

class StringTools
{
    public const  DICTIONARY_LOWERCASE    = 1;
    public const  DICTIONARY_UPPERCASE    = 2;
    public const  DICTIONARY_NUMBERS      = 4;
    public const  DICTIONARY_SYMBOLS      = 8;
    public const  DICTIONARY_ALPHANUMERIC = self::DICTIONARY_LOWERCASE | self::DICTIONARY_UPPERCASE | self::DICTIONARY_NUMBERS;
    public const  DICTIONARY_FULL         = self::DICTIONARY_ALPHANUMERIC | self::DICTIONARY_SYMBOLS;

    public static function slug(string $string): string
    {
        if (function_exists('iconv')) {
            $string = @iconv('UTF-8', 'ASCII//TRANSLIT', $string);
        }

        $string = preg_replace('/ {2,}/', ' ', $string);
        $string = str_replace(' ', '-', $string);
        $string = strtolower($string);
        $string = preg_replace('/[^a-z0-9 -]/', '', $string);

        return $string;
    }

    /**
     * Generates a random string which is of requested length.
     *
     * @param int $stringLength    How long should a generated string will be
     * @param int $dictionaryItems Which characters should be included in generated string. Default: uppercase and lowercase a-z latin characters.
     *
     * @return string
     */
    public static function generateRandomString(int $stringLength, int $dictionaryItems = self::DICTIONARY_LOWERCASE): string
    {
        $randomString = '';

        if ($stringLength < 1) {
            return $randomString;
        }

        $dictionary = '';

        if (self::DICTIONARY_LOWERCASE & $dictionaryItems) {
            $dictionary .= 'abcdefghijklmnopqrstuvwxyz';
        }

        if (self::DICTIONARY_UPPERCASE & $dictionaryItems) {
            $dictionary .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        }

        if (self::DICTIONARY_NUMBERS & $dictionaryItems) {
            $dictionary .= '0123456789';
        }

        if (self::DICTIONARY_SYMBOLS & $dictionaryItems) {
            $dictionary .= ':;-_*+=.,()';
        }

        $dictionary       = str_shuffle($dictionary);
        $dictionaryLength = strlen($dictionary) - 1;

        for ($i = 0; $i < $stringLength; $i++) {
            $randomString .= $dictionary[random_int(0, $dictionaryLength)];
        }

        return $randomString;
    }

    /**
     * Generates multiple random strings, that would be of given length and made from requested dictionary.
     *
     * @param int $stringLength    How long should a generated string will be
     * @param int $nrOfStrings     How many strings to generate
     * @param int $dictionaryItems Which characters should be included in generated string. Default: uppercase and lowercase a-z latin characters.
     *
     * @return string[]
     */
    public static function generateListOfRandomStrings(int $stringLength, int $nrOfStrings, int $dictionaryItems = self::DICTIONARY_LOWERCASE): array
    {
        $listOfRandomStrings = [];

        if ($nrOfStrings > 0 && $stringLength > 0) {
            for ($i = 0; $i < $nrOfStrings; $i++) {
                $listOfRandomStrings[] = self::generateRandomString($stringLength, $dictionaryItems);
            }
        }

        return $listOfRandomStrings;
    }

    public static function camelCaseToUnderscore(string $camelCasedString): string
    {
        $underscoredString = strtolower(
            preg_replace(
                ['/([A-Z]+)/', '/_([A-Z]+)([A-Z][a-z])/'],
                ['_$1', '_$1_$2'],
                lcfirst($camelCasedString)
            )
        );

        return trim($underscoredString, '_');
    }

    public static function underscoreToCamelCase(string $underscoredString, bool $capitalizeFirstCharacter = false): string
    {
        $camelCasedString = str_replace('_', '', ucwords($underscoredString, '_'));

        if (!$capitalizeFirstCharacter) {
            $camelCasedString = lcfirst($camelCasedString);
        }

        return $camelCasedString;
    }

    public static function stringSeparatedToWords(string $string, string $separatorSymbol = '_', bool $capitalizeAllWords = true): string
    {
        $replacedString = str_replace($separatorSymbol, ' ', ucwords($string, $separatorSymbol));

        if (!$capitalizeAllWords) {
            $replacedString = ucfirst(strtolower($replacedString));
        }

        return $replacedString;
    }

}
