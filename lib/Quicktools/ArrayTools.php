<?php

namespace Quicktools;

class ArrayTools
{

    /**
     * A very flexible solution for sorting arrays
     *
     * It's reusable: you specify the sort column as a variable instead of hardcoding it.
     * It's flexible: you can specify multiple sort columns (as many as you want) -- additional columns are used as tiebreakers between items that initially
     * compare equal. It's reversible: you can specify that the sort should be reversed -- individually for each column.
     * It's extensible: if the data set contains columns that cannot be compared in a "dumb" manner (e.g. date strings) you can also specify
     * how to convert these items to a value that can be directly compared (e.g. a DateTime instance).
     * It's associative if you want: this code takes care of sorting items, but you select the actual sort function (usort or uasort).
     * Finally, it does not use array_multisort: while array_multisort is convenient, it depends on creating a projection of all
     * your input data before sorting. This consumes time and memory and may be simply prohibitive if your data set is large.
     *
     * How to use, basic examples:
     * usort($data, comparer('name'));
     * uasort($data, comparer('name')); //it depends on you whether to use usort or uasort
     * usort($data, comparer(0)); // 0 = first numerically indexed column
     *
     * You can specify multiple sort columns by passing additional parameters to make_comparer. For example, to sort by "number" and then by the zero-indexed
     * column: usort($data, comparer('number', 0));
     *
     * @author http://stackoverflow.com/users/50079/jon
     * @link   http://stackoverflow.com/questions/96759/how-do-i-sort-a-multidimensional-array-in-php/16788610#16788610
     * @return integer
     */
    public static function comparer(): \Closure
    {
        // Normalize criteria up front so that the comparer finds everything tidy
        $criteria = func_get_args();
        foreach ($criteria as $index => $criterion) {
            $criteria[$index] = is_array($criterion)
                ? array_pad($criterion, 3, null)
                : [$criterion, SORT_ASC, null];

        }

        return static function ($first, $second) use (&$criteria): int {
            foreach ($criteria as $criterion) {
                // How will we compare this round?
                [$column, $sortOrder, $projection] = $criterion;
                $sortOrder = $sortOrder === SORT_DESC ? -1 : 1;

                // If a projection was defined project the values now
                if ($projection) {
                    $lhs = $projection($first[$column]);
                    $rhs = $projection($second[$column]);
                } else {
                    $lhs = $first[$column];
                    $rhs = $second[$column];
                }

                // Do the actual comparison; do not return if equal
                if ($lhs < $rhs) {
                    return -1 * $sortOrder;
                }

                if ($lhs > $rhs) {
                    return 1 * $sortOrder;
                }
            }

            return 0; // tiebreakers exhausted, so $first == $second
        };
    }

    public static function ksortRecursive(array &$array): void
    {
        ksort($array);
        foreach ($array as &$value) {
            if (is_array($value)) {
                self::ksortRecursive($value);
            }
        }
    }
}
