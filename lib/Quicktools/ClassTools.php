<?php

namespace Quicktools;

class ClassTools
{
    private const PROXY_MARKER        = '__CG__';
    private const PROXY_MARKER_LENGTH = 6;

    public static function getRealClass(string $className): string
    {
        // Check for proxy-ied classes that adhere
        if (false !== $pos = strrpos($className, '\\' . self::PROXY_MARKER . '\\')) {
            return substr($className, $pos + self::PROXY_MARKER_LENGTH + 2);
        }

        return $className;
    }

    public static function usesTrait(string $className, string $traitName): bool
    {
        // Try to get the real class name instead of proxy class name, because we need real one,
        // to properly use 'class_uses', as it provide information only about requested class, not its parent(s).
        $className = self::getRealClass($className);

        return isset(class_uses($className)[$traitName]);
    }

    public static function getShortClassName(string $namespacedClassName) : string
    {
        $namespacedClassNameParts = explode('/', str_replace('\\', '/', $namespacedClassName));

        return end($namespacedClassNameParts);
    }
}