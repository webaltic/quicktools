<?php

namespace Quicktools;

class DateTools
{

    /**
     * Returns next working day if the given date falls on weekend
     *
     * @param \DateTime $date
     *
     * @return \DateTime
     */
    public static function getNextWorkingDay(\DateTime $date): \DateTime
    {
        // 1 to 7
        $weekday = (int) $date->format('N');

        if ($weekday === 7) {
            $date->modify('+1 day');
        } elseif ($weekday === 6) {
            $date->modify('+2 day');
        }

        return $date;
    }

    /**
     * Add work days to given date
     *
     * @param \DateTime $date
     * @param int       $nrOfDays
     *
     * @return \DateTime
     */
    public static function addWorkDays(\DateTime $date, int $nrOfDays): \DateTime
    {
        for ($i = 0; $i < $nrOfDays; $i++) {
            $weekDay = (int) $date->format('N');

            if ($weekDay === 5) {
                $date->modify('+3 day');
            } elseif ($weekDay === 6) {
                $date->modify('+2 day');
            } else {
                $date->modify('+1 day');
            }
        }

        return $date;
    }

    /**
     * @param \DateTime $startDate
     * @param \DateTime $endDate
     * @param string    $intervalFormat
     * @param string    $outputFormat
     *
     * @return \DateTime[]
     * @throws \Exception
     */
    public static function generateDatesRange(\DateTime $startDate, \DateTime $endDate, string $intervalFormat = 'P1D', string $outputFormat = 'Y-m-d'): array
    {
        $array = [];

        $endDate->add(new \DateInterval($intervalFormat));

        if ($startDate < $endDate) {
            $period = new \DatePeriod($startDate, new \DateInterval($intervalFormat), $endDate);

            /* @var $date \DateTime */
            foreach ($period as $date) {
                $array[] = $date->format($outputFormat);
            }
        }

        return $array;
    }

    /**
     * Returns current date with microseconds inclusive as opposed to default implementation of having always '.000000'
     *
     * @return \DateTime
     */
    public static function getCurrentMicroDate(): \DateTime
    {
        $currentMicrotime = microtime(true);
        $microPart        = sprintf('%06d', ($currentMicrotime - floor($currentMicrotime)) * 1000000);

        return new \DateTime(date('Y-m-d H:i:s.' . $microPart, $currentMicrotime));
    }

    /**
     * @param \DateTimeInterface $fromDate
     * @param \DateTimeInterface $tillDate
     * @param mixed              $defaultValue
     * @param string             $format
     *
     * @return array
     */
    public static function createHourlyValues(\DateTimeInterface $fromDate, \DateTimeInterface $tillDate, $defaultValue = 0, string $format = 'd/H'): array
    {
        $periods = [];

        $interval = $tillDate->diff($fromDate);

        $intervalDays  = (int) $interval->format('%a');
        $intervalHours = (int) $interval->format('%h');
        $totalHours    = $intervalDays * 24 + $intervalHours;

        $iterableDate = $fromDate instanceof \DateTimeImmutable ? self::toMutable($fromDate) : clone $fromDate;

        for ($i = 0; $i <= $totalHours; $i++) {
            $hour           = $iterableDate->format($format);
            $periods[$hour] = $defaultValue;
            $iterableDate->add(\DateInterval::createFromDateString('1 hour'));
        }

        return $periods;
    }

    /**
     * @param \DateTimeInterface $fromDate
     * @param \DateTimeInterface $tillDate
     * @param mixed              $defaultValue
     * @param string             $format
     *
     * @return array
     */
    public static function createDailyValues(\DateTimeInterface $fromDate, \DateTimeInterface $tillDate, $defaultValue = 0, string $format = 'Y-m-d'): array
    {
        $periods = [];

        $interval       = $tillDate->diff($fromDate);
        $intervalInDays = $interval->format('%a');

        $iterableDate = $fromDate instanceof \DateTimeImmutable ? self::toMutable($fromDate) : clone $fromDate;

        for ($i = 0; $i <= $intervalInDays; $i++) {
            $day           = $iterableDate->format($format);
            $periods[$day] = $defaultValue;
            $iterableDate->add(\DateInterval::createFromDateString('1 day'));
        }

        return $periods;
    }

    /**
     * @param \DateTimeInterface $fromDate
     * @param \DateTimeInterface $tillDate
     * @param mixed              $defaultValue
     * @param string             $format
     *
     * @return array
     */
    public static function createMonthlyValues(\DateTimeInterface $fromDate, \DateTimeInterface $tillDate, $defaultValue = 0, string $format = 'Y-m'): array
    {
        $periods = [];

        $interval = $tillDate->diff($fromDate);

        $intervalYears  = (int) $interval->format('%Y');
        $intervalMonths = (int) $interval->format('%m');
        $totalMonths    = $intervalYears * 12 + $intervalMonths;

        $iterableDate = $fromDate instanceof \DateTimeImmutable ? self::toMutable($fromDate) : clone $fromDate;

        for ($i = 0; $i <= $totalMonths; $i++) {
            $month           = $iterableDate->format($format);
            $periods[$month] = $defaultValue;
            $iterableDate->add(\DateInterval::createFromDateString('1 month'));
        }

        return $periods;
    }

    /**
     * Convert immutable date to mutable. A fill-up for a missing \DateTime::createFromImmutable() functionality before it is available in PHP.
     *
     * @see https://github.com/php/php-src/pull/2484
     *
     * @param \DateTimeImmutable $dateTime
     *
     * @return \DateTime
     */
    public static function toMutable(\DateTimeImmutable $dateTime): \DateTime
    {
        return \DateTime::createFromFormat('U.u', $dateTime->format('U.u'), $dateTime->getTimezone());
    }
}
