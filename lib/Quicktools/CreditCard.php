<?php
namespace Quicktools;

/**
 * Credit card quicktool.
 *
 * Known ranges:
 *      UnionPay:       622126 - 622925, 624000 - 626999, 628200 - 628899
 *      JCB:            352800 - 358999
 *      Paypal:         601104, 650600, 650610
 *      Carte Blanche:  389000 - 389999
 *      enRoute:        201400 - 201499, 214900 - 214999
 *      Diners:         300000 - 305999, 309500 - 309599, 360000 - 369999, 380000 - 399999
 *      Discover:       601100 - 601103, 601105 - 601109, 601120 - 601149, 601174,
 *                      601177 - 601179, 601186 - 601199, 644000 - 650599, 650601 - 650609, 650611 - 659999
 *      Visa:           400000 - 499999
 *      AmEx:           340000 - 349999, 370000 - 379999
 *      Solo:           633400 - 633499, 676700 - 676799
 *      Switch:         564182, 633110, 490300 - 490399, 490500 - 490599, 491100 - 491199, 493600 - 493699,
 *                      633300 - 633399, 675900 - 675999
 *      Maestro:        500000 - 509999, 560000 - 699999 (Excluding 6***** range used by Discover)
 *      MasterCard:     510000 - 559999
 *
 * Summary:
 *  Discovery network:
 *      Carte Blanche, enRoute was acquired by Diners, which later was integrated in Discover network.
 *      This network includes partnership with JCB, UnionPay, Paypal.
 *  Mastercard network:
 *      Solo was a sister company to Switch that went under Maestro name later, which is owned by Mastercard.
 *  Visa network:
 *      Visa electron, Visa debit, Visa
 *  American Express network:
 *      American Express
 *
 * Detailed notes:
 *
 * - Diners Club International:
 *      According to Discover Network, due to an alliance with Discover, MasterCard, and Diner’s Club, as of October 2009,
 *      the IIN ranges previously used by Diner’s Club (300-305, 3095, 36, 38-39) have been retired and are “for development purposes only”.
 *      Any current Diner’s Club account numbers have been reissued from number ranges assigned to Discover.
 *      https://en.wikipedia.org/wiki/Diners_Club_International
 * - Carte Blanche:
 *      By 2005, the classic Carte Blanche card had been phased out, and only the Diners Club Carte Blanche card remained.
 *      https://en.wikipedia.org/wiki/Diners_Club_International#Carte_Blanche
 * - enRoute:
 *      enRoute was a credit card issued by Air Canada until 1992, when the airline sold its credit card division to Diners Club.
 *      https://en.wikipedia.org/wiki/EnRoute_(credit_card)
 *      https://en.wikipedia.org/wiki/Diners_Club_International#enRoute
 * - JCB:
 *      On August 23, 2006 JCB announced an alliance with the Discover Network. The two companies signed a long-term agreement that
 *      led to acceptance of Discover Network brand cards at JCB point-of-sale terminals in Japan and of JCB cards on the Discover network in the U.S.
 *      This partnership became operational in 2011.
 *      https://en.wikipedia.org/wiki/JCB_Co.,_Ltd.
 * - Paypal:
 *      In August 2012, the company announced its partnership with Discover Card to allow PayPal payments.
 *      https://en.wikipedia.org/wiki/PayPal#cite_note-forbesbarth-38
 * - Solo:
 *      Solo was a debit card in the United Kingdom introduced as a sister to the then existing Switch.
 *      The Solo card scheme was decommissioned permanently on 31 March 2011.
 *      https://en.wikipedia.org/wiki/Solo_(debit_card)
 * - Switch
 *      Switch was a debit card in the United Kingdom which was renamed Maestro by its owner MasterCard in 2002.
 *      https://en.wikipedia.org/wiki/Switch_(debit_card)
 * Links:
 * - Discover network:
 *      http://www.discovernetwork.com/value-added-reseller/
 *      https://www.discovernetwork.com/merchants/images/Merchant_Marketing.pdf
 *      http://www.discovernetwork.com/value-added-reseller/images/Discover-IIN-Bulletin-Feb-2015-FINAL.pdf
 * - IIN list for Barclays:
 *      https://webcache.googleusercontent.com/search?q=cache:evVm93Irdv0J:https://www.barclaycard.co.uk/business/files/Ranges-and-Rules-February-2016.pdf+&cd=2&hl=en&ct=clnk&gl=es
 *
 */
class CreditCard
{
    // Generic names for different CC types
    public const CC_TYPE_AMERICAN_EXPRESS = 'amex';
    public const CC_TYPE_CARTE_BLANCHE    = 'carte';
    public const CC_TYPE_DINERS_CLUB      = 'dci';
    public const CC_TYPE_DISCOVER         = 'discover';
    public const CC_TYPE_ENROUTE          = 'enroute';
    public const CC_TYPE_JCB              = 'jcb';
    public const CC_TYPE_MAESTRO          = 'maestro';
    public const CC_TYPE_MASTERCARD       = 'mc';
    public const CC_TYPE_PAYPAL           = 'paypal';
    public const CC_TYPE_SOLO             = 'solo';
    public const CC_TYPE_SWITCH           = 'switch';
    public const CC_TYPE_UNION_PAY        = 'unionpay';
    public const CC_TYPE_VISA             = 'visa';
    public const CC_TYPE_UNKNOWN          = 'unknown';

    // IIN matches for different CC types. All these should match first 6 digits!
    private const IIN_AMERICAN_EXPRESS = '3[47]\d{4}';
    private const IIN_CARTE_BLANCHE    = '389\d{3}';
    private const IIN_DINERS_CLUB      = '(30[0-5]\d|3095|3[89]\d{2}|36\d{2})\d{2}';
    private const IIN_DISCOVER         = '(6011(0[0-35-9]|[2-4]\d|7[47-9]|8[6-9]|9\d))|(644\d{3})|(6505\d{2})|(6506([0-1][1-9]|[2-9]\d))|(650[7-9]\d{2})|(65[1-9]\d{3})';
    private const IIN_ENROUTE          = '(2014|2149)\d{2}';
    private const IIN_JCB              = '35(28|29|[3-8]\d)\d{2}';
    private const IIN_MAESTRO          = '5[06-9]\d{4}|6\d{5}';
    private const IIN_MASTERCARD       = '((5[1-5]\d{4})|(222[1-9]\d{2}|22[3-9]\d{3}|2[3-6]\d{4}|27[01]\d{3}|2720\d{2}))';
    private const IIN_PAYPAL           = '(601104|650600|650610)';
    private const IIN_SOLO             = '(6334|6767)\d{2}';
    private const IIN_SWITCH           = '(564182|633110|((4903|4905|4911|4936|6333|6759)\d{2}))';
    private const IIN_UNION_PAY        = '62(212[6-9]|21[3-9]\d|2[2-8]\d{2}|291\d|292[1-5]|[4-6]\d{3}|8[2-8]\d{2})';
    private const IIN_VISA             = '4\d{5}';

    // Global masks for matching full CC number
    private const MASK_AMERICAN_EXPRESS = self::IIN_AMERICAN_EXPRESS . '\d{9}';
    private const MASK_CARTE_BLANCHE    = self::IIN_CARTE_BLANCHE . '\d{5}';
    private const MASK_DINERS_CLUB      = self::IIN_DINERS_CLUB . '\d{8,10}';
    private const MASK_DISCOVER         = self::IIN_DISCOVER . '\d{10,13}';
    private const MASK_ENROUTE          = self::IIN_ENROUTE . '\d{9}';
    private const MASK_JCB              = self::IIN_JCB . '\d{10,13}';
    private const MASK_MAESTRO          = self::IIN_MAESTRO . '\d{6,13}';
    private const MASK_MASTERCARD       = self::IIN_MASTERCARD . '\d{10}';
    private const MASK_PAYPAL           = self::IIN_PAYPAL . '\d{10,13}';
    private const MASK_SOLO             = self::IIN_SOLO . '\d{10}(\d{2,3})?';
    private const MASK_SWITCH           = self::IIN_SWITCH . '\d{10}(\d{2,3})?';
    private const MASK_UNION_PAY        = self::IIN_UNION_PAY . '\d{10,13}';
    private const MASK_VISA             = self::IIN_VISA . '\d{10}';

    /**
     * Removes all non-digit characters from the given credit card number
     *
     * @param string $ccNumber
     *
     * @return string
     */
    private static function cleanupCardNumber(string $ccNumber): string
    {
        return (string) preg_replace('/\D/', '', $ccNumber);
    }

    /**
     * Mask credit card number
     *
     * @param string $ccNumber
     * @param string $maskCharacter
     * @param int    $allowedBefore
     * @param int    $allowedAfter
     *
     * @return string
     */
    public static function mask(string $ccNumber, string $maskCharacter = '*', int $allowedBefore = 6, int $allowedAfter = 4) : string
    {
        $ccNumber = self::cleanupCardNumber($ccNumber);

        return substr($ccNumber, 0, $allowedBefore)
               . str_repeat($maskCharacter, strlen($ccNumber) - $allowedBefore - $allowedAfter)
               . substr($ccNumber, -$allowedAfter);
    }

    /**
     * Create a hash for credit card number
     *
     * @param string $ccNumber
     * @param string $salt
     * @param string $algorithm
     *
     * @return string
     */
    public static function encryptNumber(string $ccNumber, string $salt = '', string $algorithm = 'sha256'): string
    {
        $ccNumber = self::cleanupCardNumber($ccNumber);

        if ($salt) {
            $salt = md5($salt);
        }

        return hash($algorithm, $ccNumber . $salt, false);
    }

    /**
     * Detect credit card type.
     *
     * @param string $ccNumber
     *
     * @return string
     */
    public static function getExactType(string $ccNumber): string
    {
        // 'Smaller' ranges/cards/sub-brands go first, so that the card that is/was assigned to sub-brand
        // would be matched to it, instead of matching to the main brand
        $ccTypesToCheck = [
            self::CC_TYPE_CARTE_BLANCHE,
            self::CC_TYPE_ENROUTE,
            self::CC_TYPE_JCB,
            self::CC_TYPE_MAESTRO,
            self::CC_TYPE_PAYPAL,
            self::CC_TYPE_SOLO,
            self::CC_TYPE_SWITCH,
            self::CC_TYPE_UNION_PAY,
            self::CC_TYPE_DINERS_CLUB,
            self::CC_TYPE_DISCOVER,
            self::CC_TYPE_MASTERCARD,
            self::CC_TYPE_VISA,
            self::CC_TYPE_AMERICAN_EXPRESS,
        ];

        foreach ($ccTypesToCheck as $ccType) {
            if (self::isType($ccNumber, $ccType)) {
                return $ccType;
            }
        }

        return '';
    }

    /**
     * Detect credit card network.
     *
     * @param string $ccNumber
     *
     * @return string
     */
    public static function getType(string $ccNumber): ?string
    {
        foreach ([self::CC_TYPE_VISA, self::CC_TYPE_AMERICAN_EXPRESS, self::CC_TYPE_DISCOVER, self::CC_TYPE_MASTERCARD] as $network) {
            if (self::isCardNetwork($ccNumber, $network)) {
                return $network;
            }
        }

        return '';
    }

    /**
     * Check if given card number belongs to the provided cards network.
     *
     * @param string $ccNumber
     * @param string $networkName
     *
     * @return bool
     */
    public static function isCardNetwork(string $ccNumber, string $networkName): bool
    {
        if (
            $networkName === self::CC_TYPE_VISA
            && self::isType($ccNumber, self::CC_TYPE_VISA)
        ) {
            return true;
        }

        if (
            $networkName === self::CC_TYPE_AMERICAN_EXPRESS
            && self::isType($ccNumber, self::CC_TYPE_AMERICAN_EXPRESS)
        ) {
            return true;
        }

        // Discover BEFORE mastercard, as Maestro range includes some of Discover ranges, so to match c
        if (
            $networkName === self::CC_TYPE_DISCOVER
            && (
                self::isType($ccNumber, self::CC_TYPE_CARTE_BLANCHE)
                || self::isType($ccNumber, self::CC_TYPE_DINERS_CLUB)
                || self::isType($ccNumber, self::CC_TYPE_DISCOVER)
                || self::isType($ccNumber, self::CC_TYPE_ENROUTE)
                || self::isType($ccNumber, self::CC_TYPE_JCB)
                || self::isType($ccNumber, self::CC_TYPE_PAYPAL)
                || self::isType($ccNumber, self::CC_TYPE_DISCOVER)
            )
        ) {
            return true;
        }

        if (
            $networkName === self::CC_TYPE_MASTERCARD
            && (
                self::isType($ccNumber, self::CC_TYPE_SOLO)
                || self::isType($ccNumber, self::CC_TYPE_SWITCH)
                || self::isType($ccNumber, self::CC_TYPE_MAESTRO)
                || self::isType($ccNumber, self::CC_TYPE_MASTERCARD)
            )
        ) {
            return true;
        }

        return false;
    }

    /**
     * Checks if given CC number is of given CC type. If $iinMatchOnly parameter is false, will attempt to match the full given number,
     * which includes checking for length as well. If $iinMatchOnly is provided as true, will attempt to match IIN-only, which is the
     * first 6 digits, which can be useful when working with masked cards, because those would fail the full check.
     *
     * @param string $ccNumber
     * @param string $ccType
     * @param bool   $iinMatchOnly
     *
     * @return bool
     */
    public static function isType(string $ccNumber, string $ccType, bool $iinMatchOnly = true) : bool
    {
        $ccNumber = self::cleanupCardNumber($ccNumber);
        $pattern  = '';

        switch ($ccType) {
            case self::CC_TYPE_AMERICAN_EXPRESS:
                $pattern = $iinMatchOnly ? self::IIN_AMERICAN_EXPRESS : self::MASK_AMERICAN_EXPRESS;
                break;
            case self::CC_TYPE_CARTE_BLANCHE:
                $pattern = $iinMatchOnly ? self::IIN_CARTE_BLANCHE : self::MASK_CARTE_BLANCHE;
                break;
            case self::CC_TYPE_DINERS_CLUB:
                $pattern = $iinMatchOnly ? self::IIN_DINERS_CLUB : self::MASK_DINERS_CLUB;
                break;
            case self::CC_TYPE_DISCOVER:
                $pattern = $iinMatchOnly ? self::IIN_DISCOVER : self::MASK_DISCOVER;
                break;
            case self::CC_TYPE_ENROUTE:
                $pattern = $iinMatchOnly ? self::IIN_ENROUTE : self::MASK_ENROUTE;
                break;
            case self::CC_TYPE_JCB:
                $pattern = $iinMatchOnly ? self::IIN_JCB : self::MASK_JCB;
                break;
            case self::CC_TYPE_MAESTRO:
                // Check if card belongs to Discover network first, as Maestro has the full range for 6***** IIN,
                // however some of the sub-ranges are used by Discover. So to avoid complicating Maestro regex
                // the check against Discover ranges are run first and only if there's no match for it Maestro range will be checked 
                if (self::isCardNetwork($ccNumber, self::CC_TYPE_DISCOVER)) {
                    return false;
                }
                $pattern = $iinMatchOnly ? self::IIN_MAESTRO : self::MASK_MAESTRO;
                break;
            case self::CC_TYPE_MASTERCARD:
                $pattern = $iinMatchOnly ? self::IIN_MASTERCARD : self::MASK_MASTERCARD;
                break;
            case self::CC_TYPE_PAYPAL:
                $pattern = $iinMatchOnly ? self::IIN_PAYPAL : self::MASK_PAYPAL;
                break;
            case self::CC_TYPE_SOLO:
                $pattern = $iinMatchOnly ? self::IIN_SOLO : self::MASK_SOLO;
                break;
            case self::CC_TYPE_SWITCH:
                $pattern = $iinMatchOnly ? self::IIN_SWITCH : self::MASK_SWITCH;
                break;
            case self::CC_TYPE_UNION_PAY:
                $pattern = $iinMatchOnly ? self::IIN_UNION_PAY : self::MASK_UNION_PAY;
                break;
            case self::CC_TYPE_VISA:
                $pattern = $iinMatchOnly ? self::IIN_VISA : self::MASK_VISA;
                break;
        }

        if ($pattern) {
            if ($iinMatchOnly) {
                $ccNumber = substr($ccNumber, 0, 6);
            }

            return (bool) preg_match('/^' . $pattern . '$/', $ccNumber);
        }

        return false;
    }

    /**
     * Check if CC number is valid
     *
     * @var string $ccNumber
     * @return string
     */
    public static function isValid(string $ccNumber): string
    {
        // Strip any non-digits (useful for credit card numbers with spaces and hyphens)
        $ccNumber = self::cleanupCardNumber($ccNumber);

        // Set the string length and parity
        $numberLength = strlen($ccNumber);
        $parity       = $numberLength % 2;

        // Loop through each digit and do the maths
        $total = 0;
        for ($i = 0; $i < $numberLength; $i++) {
            $digit = $ccNumber[$i];
            // Multiply alternate digits by two
            if ($i % 2 == $parity) {
                $digit *= 2;
                // If the sum is two digits, add them together (in effect)
                if ($digit > 9) {
                    $digit -= 9;
                }
            }
            // Total up the digits
            $total += $digit;
        }

        // If the total mod 10 equals 0, the number is valid
        return $total % 10 == 0;
    }

    /**
     * Normalizes expiry to the form of MMYY.
     *
     * @param string $month
     * @param string $year
     *
     * @return string
     */
    public static function normalizeExpiryDate(string $month, string $year): string
    {
        $normalizedMonth = (int) $month;
        $normalizedMonth = (string) ($normalizedMonth < 10 ? '0' . $normalizedMonth : $normalizedMonth);

        $normalizedYear = (int) $year;
        $normalizedYear = strlen($normalizedYear) === 4 ? substr($normalizedYear, 2) : $normalizedYear;

        return $normalizedMonth . $normalizedYear;
    }
}
