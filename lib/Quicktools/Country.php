<?php

namespace Quicktools;

class Country
{
    public static array $countryNamesMap = [
        'en' => [
            'ad' => 'Andorra',
            'ae' => 'United Arab Emirates',
            'af' => 'Afghanistan',
            'ag' => 'Antigua and Barbuda',
            'ai' => 'Anguilla',
            'al' => 'Albania',
            'am' => 'Armenia',
            'ao' => 'Angola',
            'aq' => 'Antarctica',
            'ar' => 'Argentina',
            'as' => 'American Samoa',
            'at' => 'Austria',
            'au' => 'Australia',
            'aw' => 'Aruba',
            'ax' => 'Åland Islands',
            'az' => 'Azerbaijan',
            'ba' => 'Bosnia and Herzegovina',
            'bb' => 'Barbados',
            'bd' => 'Bangladesh',
            'be' => 'Belgium',
            'bf' => 'Burkina Faso',
            'bg' => 'Bulgaria',
            'bh' => 'Bahrain',
            'bi' => 'Burundi',
            'bj' => 'Benin',
            'bl' => 'Saint Barthélemy',
            'bm' => 'Bermuda',
            'bn' => 'Brunei Darussalam',
            'bo' => 'Bolivia, Plurinational State of',
            'bq' => 'Bonaire, Sint Eustatius and Saba',
            'br' => 'Brazil',
            'bs' => 'Bahamas',
            'bt' => 'Bhutan',
            'bv' => 'Bouvet Island',
            'bw' => 'Botswana',
            'by' => 'Belarus',
            'bz' => 'Belize',
            'ca' => 'Canada',
            'cc' => 'Cocos (Keeling) Islands',
            'cd' => 'Congo, the Democratic Republic of the',
            'cf' => 'Central African Republic',
            'cg' => 'Congo',
            'ch' => 'Switzerland',
            'ci' => "Côte d'Ivoire",
            'ck' => 'Cook Islands',
            'cl' => 'Chile',
            'cm' => 'Cameroon',
            'cn' => 'China',
            'co' => 'Colombia',
            'cr' => 'Costa Rica',
            'cu' => 'Cuba',
            'cv' => 'Cape Verde',
            'cw' => 'Curaçao',
            'cx' => 'Christmas Island',
            'cy' => 'Cyprus',
            'cz' => 'Czech Republic',
            'de' => 'Germany',
            'dj' => 'Djibouti',
            'dk' => 'Denmark',
            'dm' => 'Dominica',
            'do' => 'Dominican Republic',
            'dz' => 'Algeria',
            'ec' => 'Ecuador',
            'ee' => 'Estonia',
            'eg' => 'Egypt',
            'eh' => 'Western Sahara',
            'er' => 'Eritrea',
            'es' => 'Spain',
            'et' => 'Ethiopia',
            'fi' => 'Finland',
            'fj' => 'Fiji',
            'fk' => 'Falkland Islands (Malvinas)',
            'fm' => 'Micronesia, Federated States of',
            'fo' => 'Faroe Islands',
            'fr' => 'France',
            'ga' => 'Gabon',
            'gb' => 'United Kingdom',
            'gd' => 'Grenada',
            'ge' => 'Georgia',
            'gf' => 'French Guiana',
            'gg' => 'Guernsey',
            'gh' => 'Ghana',
            'gi' => 'Gibraltar',
            'gl' => 'Greenland',
            'gm' => 'Gambia',
            'gn' => 'Guinea',
            'gp' => 'Guadeloupe',
            'gq' => 'Equatorial Guinea',
            'gr' => 'Greece',
            'gs' => 'South Georgia and the South Sandwich Islands',
            'gt' => 'Guatemala',
            'gu' => 'Guam',
            'gw' => 'Guinea-Bissau',
            'gy' => 'Guyana',
            'hk' => 'Hong Kong',
            'hm' => 'Heard Island and McDonald Islands',
            'hn' => 'Honduras',
            'hr' => 'Croatia',
            'ht' => 'Haiti',
            'hu' => 'Hungary',
            'id' => 'Indonesia',
            'ie' => 'Ireland',
            'il' => 'Israel',
            'im' => 'Isle of Man',
            'in' => 'India',
            'io' => 'British Indian Ocean Territory',
            'iq' => 'Iraq',
            'ir' => 'Iran, Islamic Republic of',
            'is' => 'Iceland',
            'it' => 'Italy',
            'je' => 'Jersey',
            'jm' => 'Jamaica',
            'jo' => 'Jordan',
            'jp' => 'Japan',
            'ke' => 'Kenya',
            'kg' => 'Kyrgyzstan',
            'kh' => 'Cambodia',
            'ki' => 'Kiribati',
            'km' => 'Comoros',
            'kn' => 'Saint Kitts and Nevis',
            'kp' => "Korea, Democratic People's Republic of",
            'kr' => 'Korea, Republic of',
            'kw' => 'Kuwait',
            'ky' => 'Cayman Islands',
            'kz' => 'Kazakhstan',
            'la' => "Lao People's Democratic Republic",
            'lb' => 'Lebanon',
            'lc' => 'Saint Lucia',
            'li' => 'Liechtenstein',
            'lk' => 'Sri Lanka',
            'lr' => 'Liberia',
            'ls' => 'Lesotho',
            'lt' => 'Lithuania',
            'lu' => 'Luxembourg',
            'lv' => 'Latvia',
            'ly' => 'Libyan Arab Jamahiriya',
            'ma' => 'Morocco',
            'mc' => 'Monaco',
            'md' => 'Moldova, Republic of',
            'me' => 'Montenegro',
            'mf' => 'Saint Martin (French part)',
            'mg' => 'Madagascar',
            'mh' => 'Marshall Islands',
            'mk' => 'Macedonia, the former Yugoslav Republic of',
            'ml' => 'Mali',
            'mm' => 'Myanmar',
            'mn' => 'Mongolia',
            'mo' => 'Macao',
            'mp' => 'Northern Mariana Islands',
            'mq' => 'Martinique',
            'mr' => 'Mauritania',
            'ms' => 'Montserrat',
            'mt' => 'Malta',
            'mu' => 'Mauritius',
            'mv' => 'Maldives',
            'mw' => 'Malawi',
            'mx' => 'Mexico',
            'my' => 'Malaysia',
            'mz' => 'Mozambique',
            'na' => 'Namibia',
            'nc' => 'New Caledonia',
            'ne' => 'Niger',
            'nf' => 'Norfolk Island',
            'ng' => 'Nigeria',
            'ni' => 'Nicaragua',
            'nl' => 'Netherlands',
            'no' => 'Norway',
            'np' => 'Nepal',
            'nr' => 'Nauru',
            'nu' => 'Niue',
            'nz' => 'New Zealand',
            'om' => 'Oman',
            'pa' => 'Panama',
            'pe' => 'Peru',
            'pf' => 'French Polynesia',
            'pg' => 'Papua New Guinea',
            'ph' => 'Philippines',
            'pk' => 'Pakistan',
            'pl' => 'Poland',
            'pm' => 'Saint Pierre and Miquelon',
            'pn' => 'Pitcairn',
            'pr' => 'Puerto Rico',
            'ps' => 'Palestine, State of',
            'pt' => 'Portugal',
            'pw' => 'Palau',
            'py' => 'Paraguay',
            'qa' => 'Qatar',
            're' => 'Réunion',
            'ro' => 'Romania',
            'rs' => 'Serbia',
            'ru' => 'Russian Federation',
            'rw' => 'Rwanda',
            'sa' => 'Saudi Arabia',
            'sb' => 'Solomon Islands',
            'sc' => 'Seychelles',
            'sd' => 'Sudan',
            'se' => 'Sweden',
            'sg' => 'Singapore',
            'sh' => 'Saint Helena, Ascension and Tristan da Cunha',
            'si' => 'Slovenia',
            'sj' => 'Svalbard and Jan Mayen',
            'sk' => 'Slovakia',
            'sl' => 'Sierra Leone',
            'sm' => 'San Marino',
            'sn' => 'Senegal',
            'so' => 'Somalia',
            'sr' => 'Suriname',
            'ss' => 'South Sudan',
            'st' => 'Sao Tome and Principe',
            'sv' => 'El Salvador',
            'sx' => 'Sint Maarten',
            'sy' => 'Syrian Arab Republic',
            'sz' => 'Swaziland',
            'tc' => 'Turks and Caicos Islands',
            'td' => 'Chad',
            'tf' => 'French Southern Territories',
            'tg' => 'Togo',
            'th' => 'Thailand',
            'tj' => 'Tajikistan',
            'tk' => 'Tokelau',
            'tl' => 'Timor-Leste',
            'tm' => 'Turkmenistan',
            'tn' => 'Tunisia',
            'to' => 'Tonga',
            'tr' => 'Turkey',
            'tt' => 'Trinidad and Tobago',
            'tv' => 'Tuvalu',
            'tw' => 'Taiwan, Province of China',
            'tz' => 'Tanzania, United Republic of',
            'ua' => 'Ukraine',
            'ug' => 'Uganda',
            'um' => 'United States Minor Outlying Islands',
            'us' => 'United States',
            'uy' => 'Uruguay',
            'uz' => 'Uzbekistan',
            'va' => 'Holy See (Vatican City State)',
            'vc' => 'Saint Vincent and the Grenadines',
            've' => 'Venezuela, Bolivarian Republic of',
            'vg' => 'Virgin Islands, British',
            'vi' => 'Virgin Islands, U.S.',
            'vn' => 'Viet Nam',
            'vu' => 'Vanuatu',
            'wf' => 'Wallis and Futuna',
            'ws' => 'Samoa',
            'ye' => 'Yemen',
            'yt' => 'Mayotte',
            'za' => 'South Africa',
            'zm' => 'Zambia',
            'zw' => 'Zimbabwe',
        ],
        'fr' => [
            'af' => 'Afghanistan',
            'za' => 'Afrique Du Sud',
            'ax' => 'Åland, Îles',
            'al' => 'Albanie',
            'dz' => 'Algerie',
            'de' => 'Allemagne',
            'ad' => 'Andorre',
            'ao' => 'Angola',
            'ai' => 'Anguilla',
            'aq' => 'Antarctique',
            'ag' => 'Antigua Et Barbuda',
            'sa' => 'Arabie Saoudite',
            'ar' => 'Argentine',
            'am' => 'Armenie',
            'aw' => 'Aruba',
            'au' => 'Australie',
            'at' => 'Autriche',
            'az' => 'Azerbaidjan',
            'bs' => 'Bahamas',
            'bh' => 'Bahrein',
            'bd' => 'Bangladesh',
            'bb' => 'Barbade',
            'by' => 'Belarus',
            'be' => 'Belgique',
            'bz' => 'Belize',
            'bj' => 'Benin',
            'bm' => 'Bermudes',
            'bt' => 'Bhoutan',
            'bo' => 'Bolivie, L\'Etat Plurinational De',
            'bq' => 'Bonaire, Saint-Eustache Et Saba',
            'ba' => 'Bosnie-Herzegovine',
            'bw' => 'Botswana',
            'bv' => 'Bouvet, Île',
            'br' => 'Bresil',
            'bn' => 'Brunei Darussalam',
            'bg' => 'Bulgarie',
            'bf' => 'Burkina Faso',
            'bi' => 'Burundi',
            'ky' => 'Caimanes, Îles',
            'kh' => 'Cambodge',
            'cm' => 'Cameroun',
            'ca' => 'Canada',
            'cv' => 'Cap-Vert',
            'cf' => 'Centrafricaine, Republique',
            'cl' => 'Chili',
            'cn' => 'Chine',
            'cx' => 'Christmas, Île',
            'cy' => 'Chypre',
            'cc' => 'Cocos (Keeling), Îles',
            'co' => 'Colombie',
            'km' => 'Comores',
            'cg' => 'Congo',
            'cd' => 'Congo, La Republique Democratique Du',
            'ck' => 'Cook, Îles',
            'kr' => 'Coree, Republique De',
            'kp' => 'Coree, Republique Populaire Democratique De',
            'cr' => 'Costa Rica',
            'ci' => 'Cote D\'Ivoire',
            'hr' => 'Croatie',
            'cu' => 'Cuba',
            'cw' => 'Curacao',
            'dk' => 'Danemark',
            'dj' => 'Djibouti',
            'do' => 'Dominicaine, Republique',
            'dm' => 'Dominique',
            'eg' => 'Egypte',
            'sv' => 'El Salvador',
            'ae' => 'Emirats Arabes Unis',
            'ec' => 'Equateur',
            'er' => 'Erythree',
            'es' => 'Espagne',
            'ee' => 'Estonie',
            'us' => 'Etats-Unis',
            'et' => 'Ethiopie',
            'fk' => 'Falkland, Îles (Malvinas)',
            'fo' => 'Feroe, Îles',
            'fj' => 'Fidji',
            'fi' => 'Finlande',
            'fr' => 'France',
            'ga' => 'Gabon',
            'gm' => 'Gambie',
            'ge' => 'Georgie',
            'gs' => 'Georgie Du Sud Et Les Îles Sandwich Du Sud',
            'gh' => 'Ghana',
            'gi' => 'Gibraltar',
            'gr' => 'Grece',
            'gd' => 'Grenade',
            'gl' => 'Groenland',
            'gp' => 'Guadeloupe',
            'gu' => 'Guam',
            'gt' => 'Guatemala',
            'gg' => 'Guernesey',
            'gn' => 'Guinee',
            'gw' => 'Guinee-Bissau',
            'gq' => 'Guinee Equatoriale',
            'gy' => 'Guyana',
            'gf' => 'Guyane Francaise',
            'ht' => 'Haiti',
            'hm' => 'Heard, Île Et Mcdonald, Îles',
            'hn' => 'Honduras',
            'hk' => 'Hong Kong',
            'hu' => 'Hongrie',
            'im' => 'Île De Man',
            'um' => 'Îles Mineures Eloignees Des Etats-Unis',
            'vg' => 'Îles Vierges Britanniques',
            'vi' => 'Îles Vierges Des Etats-Unis',
            'in' => 'Inde',
            'id' => 'Indonesie',
            'ir' => 'Iran, Republique Islamique D\'',
            'iq' => 'Iraq',
            'ie' => 'Irlande',
            'is' => 'Islande',
            'il' => 'Israel',
            'it' => 'Italie',
            'jm' => 'Jamaique',
            'jp' => 'Japon',
            'je' => 'Jersey',
            'jo' => 'Jordanie',
            'kz' => 'Kazakhstan',
            'ke' => 'Kenya',
            'kg' => 'Kirghizistan',
            'ki' => 'Kiribati',
            'kw' => 'Koweit',
            'la' => 'Lao, Republique Democratique Populaire',
            'ls' => 'Lesotho',
            'lv' => 'Lettonie',
            'lb' => 'Liban',
            'lr' => 'Liberia',
            'ly' => 'Libyenne, Jamahiriya Arabe',
            'li' => 'Liechtenstein',
            'lt' => 'Lituanie',
            'lu' => 'Luxembourg',
            'mo' => 'Macao',
            'mk' => 'Macedoine, L\'Ex-Republique Yougoslave De',
            'mg' => 'Madagascar',
            'my' => 'Malaisie',
            'mw' => 'Malawi',
            'mv' => 'Maldives',
            'ml' => 'Mali',
            'mt' => 'Malte',
            'mp' => 'Mariannes Du Nord, Îles',
            'ma' => 'Maroc',
            'mh' => 'Marshall, Îles',
            'mq' => 'Martinique',
            'mu' => 'Maurice',
            'mr' => 'Mauritanie',
            'yt' => 'Mayotte',
            'mx' => 'Mexique',
            'fm' => 'Micronesie, Etats Federes De',
            'md' => 'Moldova, Republique De',
            'mc' => 'Monaco',
            'mn' => 'Mongolie',
            'me' => 'Montenegro',
            'ms' => 'Montserrat',
            'mz' => 'Mozambique',
            'mm' => 'Myanmar',
            'na' => 'Namibie',
            'nr' => 'Nauru',
            'np' => 'Nepal',
            'ni' => 'Nicaragua',
            'ne' => 'Niger',
            'ng' => 'Nigeria',
            'nu' => 'Niue',
            'nf' => 'Norfolk, Île',
            'no' => 'Norvege',
            'nc' => 'Nouvelle-Caledonie',
            'nz' => 'Nouvelle-Zelande',
            'io' => 'Ocean Indien, Territoire Britannique De L\'',
            'om' => 'Oman',
            'ug' => 'Ouganda',
            'uz' => 'Ouzbekistan',
            'pk' => 'Pakistan',
            'pw' => 'Palaos',
            'ps' => 'Palestinien Occupe, Territoire',
            'pa' => 'Panama',
            'pg' => 'Papouasie-Nouvelle-Guinee',
            'py' => 'Paraguay',
            'nl' => 'Pays-Bas',
            'pe' => 'Perou',
            'ph' => 'Philippines',
            'pn' => 'Pitcairn',
            'pl' => 'Pologne',
            'pf' => 'Polynesie Francaise',
            'pr' => 'Porto Rico',
            'pt' => 'Portugal',
            'qa' => 'Qatar',
            're' => 'Reunion',
            'ro' => 'Roumanie',
            'gb' => 'Royaume-Uni',
            'ru' => 'Russie, Federation De',
            'rw' => 'Rwanda',
            'eh' => 'Sahara Occidental',
            'bl' => 'Saint-Barthelemy',
            'sh' => 'Sainte-Helene, Ascension Et Tristan Da Cunha',
            'lc' => 'Sainte-Lucie',
            'kn' => 'Saint-Kitts-Et-Nevis',
            'sm' => 'Saint-Marin',
            'mf' => 'Saint-Martin (Partie Francaise)',
            'sx' => 'Saint-Martin (Partie Neerlandaise)',
            'pm' => 'Saint-Pierre-Et-Miquelon',
            'va' => 'Saint-Siege (Etat De La Cite Du Vatican)',
            'vc' => 'Saint-Vincent-Et-Les Grenadines',
            'sb' => 'Salomon, Îles',
            'ws' => 'Samoa',
            'as' => 'Samoa Americaines',
            'st' => 'Sao Tome-Et-Principe',
            'sn' => 'Senegal',
            'rs' => 'Serbie',
            'sc' => 'Seychelles',
            'sl' => 'Sierra Leone',
            'sg' => 'Singapour',
            'sk' => 'Slovaquie',
            'si' => 'Slovenie',
            'so' => 'Somalie',
            'sd' => 'Soudan',
            'lk' => 'Sri Lanka',
            'se' => 'Suede',
            'ch' => 'Suisse',
            'sr' => 'Suriname',
            'sj' => 'Svalbard Et Île Jan Mayen',
            'sz' => 'Swaziland',
            'sy' => 'Syrienne, Republique Arabe',
            'tj' => 'Tadjikistan',
            'tw' => 'Taiwan, Province De Chine',
            'tz' => 'Tanzanie, Republique-Unie De',
            'td' => 'Tchad',
            'cz' => 'Tcheque, Republique',
            'tf' => 'Terres Australes Francaises',
            'th' => 'Thailande',
            'tl' => 'Timor-Leste',
            'tg' => 'Togo',
            'tk' => 'Tokelau',
            'to' => 'Tonga',
            'tt' => 'Trinite-Et-Tobago',
            'tn' => 'Tunisie',
            'tm' => 'Turkmenistan',
            'tc' => 'Turks Et Caiques, Îles',
            'tr' => 'Turquie',
            'tv' => 'Tuvalu',
            'ua' => 'Ukraine',
            'uy' => 'Uruguay',
            'vu' => 'Vanuatu',
            've' => 'Venezuela, Republique Bolivarienne Du',
            'vn' => 'Viet Nam',
            'wf' => 'Wallis Et Futuna',
            'ye' => 'Yemen',
            'zm' => 'Zambie',
            'zw' => 'Zimbabwe',
        ],
    ];

    // List of ISO 2 char name to 3 char name
    public static array $countryCodesMap = [
        'af' => 'afg',
        'ax' => 'ala',
        'al' => 'alb',
        'dz' => 'dza',
        'as' => 'asm',
        'ad' => 'and',
        'ao' => 'ago',
        'ai' => 'aia',
        'aq' => 'ata',
        'ag' => 'atg',
        'ar' => 'arg',
        'am' => 'arm',
        'aw' => 'abw',
        'au' => 'aus',
        'at' => 'aut',
        'az' => 'aze',
        'bs' => 'bhs',
        'bh' => 'bhr',
        'bd' => 'bgd',
        'bb' => 'brb',
        'by' => 'blr',
        'be' => 'bel',
        'bz' => 'blz',
        'bj' => 'ben',
        'bm' => 'bmu',
        'bt' => 'btn',
        'bo' => 'bol',
        'ba' => 'bih',
        'bw' => 'bwa',
        'bv' => 'bvt',
        'br' => 'bra',
        'vg' => 'vgb',
        'io' => 'iot',
        'bn' => 'brn',
        'bg' => 'bgr',
        'bf' => 'bfa',
        'bi' => 'bdi',
        'kh' => 'khm',
        'cm' => 'cmr',
        'ca' => 'can',
        'cv' => 'cpv',
        'ky' => 'cym',
        'cf' => 'caf',
        'td' => 'tcd',
        'cl' => 'chl',
        'cn' => 'chn',
        'hk' => 'hkg',
        'mo' => 'mac',
        'cx' => 'cxr',
        'cc' => 'cck',
        'co' => 'col',
        'km' => 'com',
        'cg' => 'cog',
        'cd' => 'cod',
        'ck' => 'cok',
        'cr' => 'cri',
        'ci' => 'civ',
        'hr' => 'hrv',
        'cu' => 'cub',
        'cy' => 'cyp',
        'cz' => 'cze',
        'dk' => 'dnk',
        'dj' => 'dji',
        'dm' => 'dma',
        'do' => 'dom',
        'ec' => 'ecu',
        'eg' => 'egy',
        'sv' => 'slv',
        'gq' => 'gnq',
        'er' => 'eri',
        'ee' => 'est',
        'et' => 'eth',
        'fk' => 'flk',
        'fo' => 'fro',
        'fj' => 'fji',
        'fi' => 'fin',
        'fr' => 'fra',
        'gf' => 'guf',
        'pf' => 'pyf',
        'tf' => 'atf',
        'ga' => 'gab',
        'gm' => 'gmb',
        'ge' => 'geo',
        'de' => 'deu',
        'gh' => 'gha',
        'gi' => 'gib',
        'gr' => 'grc',
        'gl' => 'grl',
        'gd' => 'grd',
        'gp' => 'glp',
        'gu' => 'gum',
        'gt' => 'gtm',
        'gg' => 'ggy',
        'gn' => 'gin',
        'gw' => 'gnb',
        'gy' => 'guy',
        'ht' => 'hti',
        'hm' => 'hmd',
        'va' => 'vat',
        'hn' => 'hnd',
        'hu' => 'hun',
        'is' => 'isl',
        'in' => 'ind',
        'id' => 'idn',
        'ir' => 'irn',
        'iq' => 'irq',
        'ie' => 'irl',
        'im' => 'imn',
        'il' => 'isr',
        'it' => 'ita',
        'jm' => 'jam',
        'jp' => 'jpn',
        'je' => 'jey',
        'jo' => 'jor',
        'kz' => 'kaz',
        'ke' => 'ken',
        'ki' => 'kir',
        'kp' => 'prk',
        'kr' => 'kor',
        'kw' => 'kwt',
        'kg' => 'kgz',
        'la' => 'lao',
        'lv' => 'lva',
        'lb' => 'lbn',
        'ls' => 'lso',
        'lr' => 'lbr',
        'ly' => 'lby',
        'li' => 'lie',
        'lt' => 'ltu',
        'lu' => 'lux',
        'mk' => 'mkd',
        'mg' => 'mdg',
        'mw' => 'mwi',
        'my' => 'mys',
        'mv' => 'mdv',
        'ml' => 'mli',
        'mt' => 'mlt',
        'mh' => 'mhl',
        'mq' => 'mtq',
        'mr' => 'mrt',
        'mu' => 'mus',
        'yt' => 'myt',
        'mx' => 'mex',
        'fm' => 'fsm',
        'md' => 'mda',
        'mc' => 'mco',
        'mn' => 'mng',
        'me' => 'mne',
        'ms' => 'msr',
        'ma' => 'mar',
        'mz' => 'moz',
        'mm' => 'mmr',
        'na' => 'nam',
        'nr' => 'nru',
        'np' => 'npl',
        'nl' => 'nld',
        'an' => 'ant',
        'nc' => 'ncl',
        'nz' => 'nzl',
        'ni' => 'nic',
        'ne' => 'ner',
        'ng' => 'nga',
        'nu' => 'niu',
        'nf' => 'nfk',
        'mp' => 'mnp',
        'no' => 'nor',
        'om' => 'omn',
        'pk' => 'pak',
        'pw' => 'plw',
        'ps' => 'pse',
        'pa' => 'pan',
        'pg' => 'png',
        'py' => 'pry',
        'pe' => 'per',
        'ph' => 'phl',
        'pn' => 'pcn',
        'pl' => 'pol',
        'pt' => 'prt',
        'pr' => 'pri',
        'qa' => 'qat',
        're' => 'reu',
        'ro' => 'rou',
        'ru' => 'rus',
        'rw' => 'rwa',
        'bl' => 'blm',
        'sh' => 'shn',
        'kn' => 'kna',
        'lc' => 'lca',
        'mf' => 'maf',
        'pm' => 'spm',
        'vc' => 'vct',
        'ws' => 'wsm',
        'sm' => 'smr',
        'st' => 'stp',
        'sa' => 'sau',
        'sn' => 'sen',
        'rs' => 'srb',
        'sc' => 'syc',
        'sl' => 'sle',
        'sg' => 'sgp',
        'sk' => 'svk',
        'si' => 'svn',
        'sb' => 'slb',
        'so' => 'som',
        'za' => 'zaf',
        'gs' => 'sgs',
        'ss' => 'ssd',
        'es' => 'esp',
        'lk' => 'lka',
        'sd' => 'sdn',
        'sr' => 'sur',
        'sj' => 'sjm',
        'sz' => 'swz',
        'se' => 'swe',
        'ch' => 'che',
        'sy' => 'syr',
        'tw' => 'twn',
        'tj' => 'tjk',
        'tz' => 'tza',
        'th' => 'tha',
        'tl' => 'tls',
        'tg' => 'tgo',
        'tk' => 'tkl',
        'to' => 'ton',
        'tt' => 'tto',
        'tn' => 'tun',
        'tr' => 'tur',
        'tm' => 'tkm',
        'tc' => 'tca',
        'tv' => 'tuv',
        'ug' => 'uga',
        'ua' => 'ukr',
        'ae' => 'are',
        'gb' => 'gbr',
        'us' => 'usa',
        'um' => 'umi',
        'uy' => 'ury',
        'uz' => 'uzb',
        'vu' => 'vut',
        've' => 'ven',
        'vn' => 'vnm',
        'vi' => 'vir',
        'wf' => 'wlf',
        'eh' => 'esh',
        'ye' => 'yem',
        'zm' => 'zmb',
        'zw' => 'zwe',
    ];

    // List of locales
    public static array $localesList = [
        'af-ZA',
        'am-ET',
        'ar-AE',
        'ar-BH',
        'ar-DZ',
        'ar-EG',
        'ar-IQ',
        'ar-JO',
        'ar-KW',
        'ar-LB',
        'ar-LY',
        'ar-MA',
        'arn-CL',
        'ar-OM',
        'ar-QA',
        'ar-SA',
        'ar-SY',
        'ar-TN',
        'ar-YE',
        'as-IN',
        'az-Cyrl-AZ',
        'az-Latn-AZ',
        'ba-RU',
        'be-BY',
        'bg-BG',
        'bn-BD',
        'bn-IN',
        'bo-CN',
        'br-FR',
        'bs-Cyrl-BA',
        'bs-Latn-BA',
        'ca-ES',
        'co-FR',
        'cs-CZ',
        'cy-GB',
        'da-DK',
        'de-AT',
        'de-CH',
        'de-DE',
        'de-LI',
        'de-LU',
        'dsb-DE',
        'dv-MV',
        'el-GR',
        'en-029',
        'en-AU',
        'en-BZ',
        'en-CA',
        'en-GB',
        'en-IE',
        'en-IN',
        'en-JM',
        'en-MY',
        'en-NZ',
        'en-PH',
        'en-SG',
        'en-TT',
        'en-US',
        'en-ZA',
        'en-ZW',
        'es-AR',
        'es-BO',
        'es-CL',
        'es-CO',
        'es-CR',
        'es-DO',
        'es-EC',
        'es-ES',
        'es-GT',
        'es-HN',
        'es-MX',
        'es-NI',
        'es-PA',
        'es-PE',
        'es-PR',
        'es-PY',
        'es-SV',
        'es-US',
        'es-UY',
        'es-VE',
        'et-EE',
        'eu-ES',
        'fa-IR',
        'fi-FI',
        'fil-PH',
        'fo-FO',
        'fr-BE',
        'fr-CA',
        'fr-CH',
        'fr-FR',
        'fr-LU',
        'fr-MC',
        'fy-NL',
        'ga-IE',
        'gd-GB',
        'gl-ES',
        'gsw-FR',
        'gu-IN',
        'ha-Latn-NG',
        'he-IL',
        'hi-IN',
        'hr-BA',
        'hr-HR',
        'hsb-DE',
        'hu-HU',
        'hy-AM',
        'id-ID',
        'ig-NG',
        'ii-CN',
        'is-IS',
        'it-CH',
        'it-IT',
        'iu-Cans-CA',
        'iu-Latn-CA',
        'ja-JP',
        'ka-GE',
        'kk-KZ',
        'kl-GL',
        'km-KH',
        'kn-IN',
        'kok-IN',
        'ko-KR',
        'ky-KG',
        'lb-LU',
        'lo-LA',
        'lt-LT',
        'lv-LV',
        'mi-NZ',
        'mk-MK',
        'ml-IN',
        'mn-MN',
        'mn-Mong-CN',
        'moh-CA',
        'mr-IN',
        'ms-BN',
        'ms-MY',
        'mt-MT',
        'nb-NO',
        'ne-NP',
        'nl-BE',
        'nl-NL',
        'nn-NO',
        'nso-ZA',
        'oc-FR',
        'or-IN',
        'pa-IN',
        'pl-PL',
        'prs-AF',
        'ps-AF',
        'pt-BR',
        'pt-PT',
        'qut-GT',
        'quz-BO',
        'quz-EC',
        'quz-PE',
        'rm-CH',
        'ro-RO',
        'ru-RU',
        'rw-RW',
        'sah-RU',
        'sa-IN',
        'se-FI',
        'se-NO',
        'se-SE',
        'si-LK',
        'sk-SK',
        'sl-SI',
        'sma-NO',
        'sma-SE',
        'smj-NO',
        'smj-SE',
        'smn-FI',
        'sms-FI',
        'sq-AL',
        'sr-Cyrl-BA',
        'sr-Cyrl-CS',
        'sr-Cyrl-ME',
        'sr-Cyrl-RS',
        'sr-Latn-BA',
        'sr-Latn-CS',
        'sr-Latn-ME',
        'sr-Latn-RS',
        'sv-FI',
        'sv-SE',
        'sw-KE',
        'syr-SY',
        'ta-IN',
        'te-IN',
        'tg-Cyrl-TJ',
        'th-TH',
        'tk-TM',
        'tn-ZA',
        'tr-TR',
        'tt-RU',
        'tzm-Latn-DZ',
        'ug-CN',
        'uk-UA',
        'ur-PK',
        'uz-Cyrl-UZ',
        'uz-Latn-UZ',
        'vi-VN',
        'wo-SN',
        'xh-ZA',
        'yo-NG',
        'zh-CN',
        'zh-HK',
        'zh-MO',
        'zh-SG',
        'zh-TW',
        'zu-ZA',
    ];

    // List of US territories
    public static array $usStatesListExtra = [
        'as' => 'American Samoa',
        'dc' => 'District Of Columbia',
        'gu' => 'Guam Gu',
        'mh' => 'Marshall Islands',
        'mp' => 'Northern Mariana Islands',
        'pw' => 'Palau',
        'pr' => 'Puerto Rico',
        'vi' => 'Virgin Islands',
    ];

    // List of US states
    public static array $usStatesList = [
        'al' => 'Alabama',
        'ak' => 'Alaska',
        'az' => 'Arizona',
        'ar' => 'Arkansas',
        'ca' => 'California',
        'co' => 'Colorado',
        'ct' => 'Connecticut',
        'de' => 'Delaware',
        'fm' => 'Federated States Of Micronesia',
        'fl' => 'Florida',
        'ga' => 'Georgia',
        'hi' => 'Hawaii',
        'id' => 'Idaho',
        'il' => 'Illinois',
        'in' => 'Indiana',
        'ia' => 'Iowa',
        'ks' => 'Kansas',
        'ky' => 'Kentucky',
        'la' => 'Louisiana',
        'me' => 'Maine',
        'md' => 'Maryland',
        'ma' => 'Massachusetts',
        'mi' => 'Michigan',
        'mn' => 'Minnesota',
        'ms' => 'Mississippi',
        'mo' => 'Missouri',
        'mt' => 'Montana',
        'ne' => 'Nebraska',
        'nv' => 'Nevada',
        'nh' => 'New Hampshire',
        'nj' => 'New Jersey',
        'nm' => 'New Mexico',
        'ny' => 'New York',
        'nc' => 'North Carolina',
        'nd' => 'North Dakota',
        'oh' => 'Ohio',
        'ok' => 'Oklahoma',
        'or' => 'Oregon',
        'pa' => 'Pennsylvania',
        'ri' => 'Rhode Island',
        'sc' => 'South Carolina',
        'sd' => 'South Dakota',
        'tn' => 'Tennessee',
        'tx' => 'Texas',
        'ut' => 'Utah',
        'vt' => 'Vermont',
        'va' => 'Virginia',
        'wa' => 'Washington',
        'wv' => 'West Virginia',
        'wi' => 'Wisconsin',
        'wy' => 'Wyoming',
    ];

    /**
     * Convert 2 characters ISO country code to a country name
     *
     * @param string $isoCode
     * @param string $nameLanguage
     *
     * @return string
     */
    public static function iso2ToName(string $isoCode, string $nameLanguage = 'en'): string
    {
        $isoCode      = strtolower($isoCode);
        $nameLanguage = strtolower($nameLanguage);

        return self::$countryNamesMap[$nameLanguage][$isoCode] ?? $isoCode;
    }

    /**
     * Returns a list of country names
     *
     * @param string      $nameLanguage
     * @param string|null $fallbackLanguage
     *
     * @return array
     */
    public static function getCountryNamesList(string $nameLanguage = 'en', ?string $fallbackLanguage = 'en'): array
    {
        $map = [];
        $nameLanguage = strtolower($nameLanguage);
        $fallbackLanguage = $fallbackLanguage ? strtolower($fallbackLanguage) : '';

        if (isset(self::$countryNamesMap[$nameLanguage])) {
            $map = self::$countryNamesMap[$nameLanguage];
        } elseif (isset(self::$countryNamesMap[$fallbackLanguage])) {
            $map = self::$countryNamesMap[$fallbackLanguage];
        }

        return $map;
    }

    /**
     * Returns a list of US states names
     *
     * @param bool $extendedList Should an extended list be returned. A normal list consists of 50 US states,
     *                           extended one adds few overseas territories like Puerto Rico.
     *
     * @return array
     */
    public static function getUsStatesNamesList(bool $extendedList = false): array
    {
        $statesList = self::$usStatesList;

        if ($extendedList) {
            $statesList += self::$usStatesListExtra;
            sort($statesList);
        }

        return $statesList;
    }

    /**
     * Returns an ISO_3166-1_alpha-3 (3 letters) country code for a ISO 3166-1 alpha-2 (2 letters) country code
     *
     * @param string $iso2Code
     *
     * @return string
     */
    public static function iso2ToIso3(string $iso2Code): string
    {
        $iso2Code = strtolower($iso2Code);

        return self::$countryCodesMap[$iso2Code] ?? '';
    }

    /**
     * Returns an ISO_3166-1_alpha-2 (2 letters) country code for a ISO 3166-1 alpha-3 (3 letters) country code
     *
     * @param string $iso3Code
     *
     * @return string
     */
    public static function iso3ToIso2(string $iso3Code): string
    {
        $iso3Code        = strtolower($iso3Code);
        $countryCodesMap = array_flip(self::$countryCodesMap);

        return $countryCodesMap[$iso3Code] ?? '';
    }

    /**
     * Get full locale name based on given country, and optionally - language
     *
     * @param string $countryCode
     * @param string $languageCode
     *
     * @return string
     */
    public static function getLocaleByCountry(string $countryCode, string $languageCode = ''): string
    {
        foreach (self::$localesList as $locale) {
            $localeRegion   = locale_get_region($locale);
            $localeLanguage = locale_get_primary_language($locale);
            $localeParts    = [
                'language' => $localeLanguage,
                'region'   => $localeRegion,
            ];

            if (
                strtoupper($countryCode) === $localeRegion
                && ($languageCode === '' || strtolower($languageCode) === $localeLanguage)
            ) {
                return locale_compose($localeParts);
            }
        }

        return $countryCode;
    }
}